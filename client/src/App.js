import React, { Component } from 'react';
import './App.css';
import Spotify from 'spotify-web-api-js';

const spotifyWebApi = new Spotify();
var offSetCounter = 0;
var ArtistArray = [];
var SongArray = [];
var GenreArray = [];

function getSavedTracks(offset, limit) {
  spotifyWebApi.getMySavedTracks({ offset: offset, limit: limit })

    .then(function (data) {
      var artistString = "";

      if (offSetCounter + 50 > data.total) { //i.e. we need to add less than 50 artists 
        // build a string of artists      
        for (var i = 0; i < data.total - offSetCounter; i++) {
          SongArray.push(data.items[i].track.name);
          artistString += data.items[i].track.artists[0].id + ",";
        }
        // add that string to an array
        artistString = artistString.slice(0, -1);
        ArtistArray.push(artistString);
        // we have read all their saved tracks
        window.alert("Saved tracks have been read!");
        return;
      }
      else {
        // builds a string of artists
        for (i = 0; i < 50; i++) {
          SongArray.push(data.items[i].track.name);
          artistString += data.items[i].track.artists[0].id.toString() + ",";
        }
        // add that string to an array
        artistString = artistString.slice(0, -1);
        ArtistArray.push(artistString);
      }

      offSetCounter += 50;
      getSavedTracks(offSetCounter, 50);
    }, function (err) {
      console.error(err);
    });
}



function getArtistGenre() {
  for (var i = 0; i < ArtistArray.length; i++) {
        setTimeout(temp(i),500);
  }
  window.alert("Genres have been read!")
}

function printArrays(){
      for (var i=0;i<GenreArray.length;i++){
        console.log(i+1+")"+SongArray[i]+"--->"+GenreArray[i]);
      }
}

function temp(i){
  var tempString = ArtistArray[i];
    // get multiple artists
    
    spotifyWebApi.getArtists([tempString])
      .then(function (data) {
        for(var i=0;i<50;i++){
          try {
            GenreArray.push(data.artists[i].genres[0]);
          } catch (error) {
            console.log("Problemmmm");
          }
          
          
        }
      }, function (err) {
        console.error(err);
      });
}

class App extends Component {
  constructor() {
    super();
    const params = this.getHashParams();
    this.state = {
      loggedIn: params.access_token ? true : false,
    }
    if (params.access_token) {
      spotifyWebApi.setAccessToken(params.access_token);
    }
  }

  getHashParams() {
    var hashParams = {};
    var e, r = /([^&;=]+)=?([^&;]*)/g,
      q = window.location.hash.substring(1);
    while (e = r.exec(q)) {
      hashParams[e[1]] = decodeURIComponent(e[2]);
    }
    return hashParams;
  }

  render() {
    return (
      <div className="App">
        <a href='http://localhost:8888/'>
          <button>Login With Spotify</button>
        </a>
        <button onClick={() => getSavedTracks(0, 50)}>
          Check Saved Tracks
        </button>
        <button onClick={() => getArtistGenre()}>
          Check Genres
        </button>
        <button onClick={() => printArrays()}>
          Print Test
        </button>
      </div>

    );
  }
}

export default App;
